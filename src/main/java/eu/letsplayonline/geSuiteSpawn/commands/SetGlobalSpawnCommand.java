package eu.letsplayonline.geSuiteSpawn.commands;

import eu.letsplayonline.geSuiteSpawn.managers.SpawnManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class SetGlobalSpawnCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {

		SpawnManager.setProxySpawn(sender);
		return true;

	}

}
